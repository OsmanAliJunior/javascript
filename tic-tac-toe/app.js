var players = ["Ali", "PC"];
var markers = ["X", "0"];
var scores = [0, 0];
var activePlayer = 0; //0 for Ali and 1 for PC
var gameOver = false;
// winning combination, converted from binary to decimal, sum of rows&col
//primary diagonal and secondory diagonal
var winValues = [7, 56, 73, 84, 146, 273, 292, 448];
// 9 boxes, each box has own value(binary converted to decimal)


// score calculation
function pointCount(userPoints){
  scores[activePlayer] = scores[activePlayer] + userPoints;
}

// Win combos
function checkWinner(){
//  console.log(scores[0] + " " + scores[1]);
  for(var i = 0; i < winValues.length; i++){
    if((winValues[i] & scores[activePlayer]) == winValues[i]){ // binary &
      gameOver = true;
      document.getElementById('main').innerText = players[activePlayer] + " Wins!";
      //alert(players[activePlayer] + " you win");
    }
  }
}



function play(clickedDiv, divPoints){
  if(!gameOver){
    if(clickedDiv.innerHTML == '&nbsp;'){
      pointCount(divPoints);
      clickedDiv.innerHTML = markers[activePlayer];
      checkWinner();
        togglePlayer();

    }else {
      togglePlayer();
    }
  }
}

//alert(players[activePlayer]);

function togglePlayer(){
  if(activePlayer == 0)  activePlayer = 1;
  else activePlayer = 0;

document.getElementById("turn").innerText = players[activePlayer] + "'s TURN";
}


//from google ->> &nbsp is none-breaking-space
//&nbsp-help us to avoid overwriting X or 0, if X was written
//you can not write 0





//Play Again button
/**  function playAgain(){


  } */
