//creating objects using the function constructor
/**
var john = {
  name: 'John',
  yearOfBirth: 1990,
  job: 'teacher'
};

  var Person = function (name, yearOfBirth,job) {
    this.name = name;
    this.yearOfBirth = yearOfBirth;
    this.job = job;

  }

  Person.prototype.calculateAge = function () {  //create a method that any other created objects inherits it
    console.log(2018 - this.yearOfBirth);
  };

  Person.prototype.lastName = 'Smith';
  var john = new Person('John', 1990, 'teacher'); //instantiating


var jane = new Person('Jane', 1969, 'designer');
var mark = new Person('Mark', 1948, 'retired');

john.calculateAge();
jane.calculateAge();
mark.calculateAge();


console.log(john.lastName);
console.log(mark.lastName);
console.log(jane.lastName); **/

//Object create
  /** var personProto = {
  calculateAge: function() {
    console.log(2018 - this.yearOfBirth);
  }
};

var john = Object.create(personProto);
john.name = "John";
john.yearOfBirth = 1990;
john.job = 'teacher';**/
/*
(function() {

          function Question(question, answers, correctAnswer) {
          this.question = question;
          this.answers = answers;
          this.correctAnswer = correctAnswer;
          }

          Question.prototype.displayQuestion = function(){
            console.log(this.question);
              for(var i = 0; i < this.answers.length; i++){
                  console.log(i + ": " + this.answers[i]);
              }
          }

            Question.prototype.checkAnswer = function(ans) {
              if(ans === this.correctAnswer){
                console.log("correct answer!");
              } else {
                console.log("Try again. Wrong answer");
                }
            }


        var question1 = new Question('Will you hack the world?',
                                                        ['yes', 'no'],
                                                              0);
        var question2 = new Question("Which programming language do you like?",
                                                      ["all of them", "none of them"],
                                                       0);

        var question3 = new Question("What does best describe coding?",
                                                  ["boring",
                                                  "interesting",
                                                  "very Interesting"],
                                                   2);
      var questions = [question1, question2, question3];
      var randomNumber = Math.floor(Math.random() * questions.length);

      questions[randomNumber].displayQuestion();


      var answer = parseInt(prompt("Please select the correct answer"));


    questions[randomNumber].checkAnswer(answer);

}) ();*/




/*
(function() {

          function Question(question, answers, correctAnswer) {
          this.question = question;
          this.answers = answers;
          this.correctAnswer = correctAnswer;
          }

          Question.prototype.displayQuestion = function(){  //wiriting methods
            console.log(this.question);
              for(var i = 0; i < this.answers.length; i++){
                  console.log(i + ": " + this.answers[i]);
              }
          }

            Question.prototype.checkAnswer = function(ans, callback) {
                var sc;
              if(ans === this.correctAnswer){
                console.log("correct answer!");
                sc = callback(true);
              } else {
                console.log("Try again. Wrong answer");
                sc = callback(false);
                }
                  this.displayQuestion(sc);
            }

              Question.prototype.displayScore = function(score) {
                console.log("your current score is: " + score );
                console.log("---------------------------------------")
              }



        var question1 = new Question('Will you hack the world?',
                                                        ['yes', 'no'],
                                                              0);
        var question2 = new Question("Which programming language do you like?",
                                                      ["all of them", "none of them"],
                                                       0);

        var question3 = new Question("What does best describe coding?",
                                                  ["boring",
                                                  "interesting",
                                                  "very Interesting"],
                                                   2);
          function nextQuestion(){
            var questions = [question1, question2, question3];


            function score() {
              var sc = 0;
              return function(correct) {
                if(correct) {
                  sc++;
                }
                    return score;
              }
            }

            var keepScore = score();
            var randomNumber = Math.floor(Math.random() * questions.length);

            questions[randomNumber].displayQuestion();


            var answer = parseInt(prompt("Please select the correct answer"));


          questions[randomNumber].checkAnswer(answer);

          if(answer !== 'exit'){
            questions[randomNumber].checkAnswer(parseInt(answer), keepScore);
          }

          nextQuestion();
          }
          nextQuestion();

}) (); */


// PASSING FUNCTIONS AS ARGUMENTS
/*
var years = [1990, 1965, 1937, 2005, 1998];

//funtion that receive an array and return a new result an array
function arrayCalc(arr, fn){
  var arrResult = [];
  for(var i = 0; i < arr.length; i++){
    arrResult.push(fn(arr[i]));
  }
  return arrResult;
}

  function calculateAge(el) {
    return 2018 - el;
  }

  function isFullAge(el){
    return el >= 18;
  }

  function maxHeartRate(el){
    if(el >= 18 && el <= 81){
    return Math.round(206.9 - (0.67 * el));
  } else {
    return -1;
  }
}


  var ages  = arrayCalc(years, calculateAge);
  var fullAges = arrayCalc(ages, isFullAge);
  var hearRate= arrayCalc(ages, maxHeartRate);
  console.log(ages);
  console.log(fullAges);
  console.log(hearRate);
**/

//FUNCTIONS RETURNING FUNCTIONS;
// Create a function that creates a different interview questions for different jobs
// Hint: for each job we will return a function that builds a string using person;s name as an input. funtion returnin another function

/*
  function interviewQuestion(job){
    if(job === 'designer'){
      return function(name){
        console.log(name + ', can you please explain what UX design is?');
      }
    } else if(job === 'teacher'){
        return function(name){
          console.log('What subject do you teach, ' + name + '?');
        }
      }else {
        return function(name){
          console.log("Hello " + name + ', what do you do?');
        }
      }
  }


/*    var teacherQuestion = interviewQuestion('teacher');
    var designerQuestion = interviewQuestion('designer');
    var anythingElse = interviewQuestion();
    teacherQuestion('Suka');
    designerQuestion('Jelep');
    anythingElse('gandon'); */
  /*  interviewQuestion('teacher')('Suka');  // This is evaluated from left to write
    interviewQuestion('designer')('Jelep');
    interviewQuestion()('Gandon'); */


//IIFE
    /*function game(){    // it is a normal function declaration
      var randomScore = Math.random() * 10;
      console.log(randomScore >=5);
    }
    game();*/

    (function(){
      var randomScore = Math.random() * 10;
      console.log(randomScore >= 5);
    })(); // () invokes the function


      (function(goodLuck){
        var score = Math.random() * 10;
        console.log(score >= 5 - goodLuck )
      })(5); // IIFE, we passed an argument "googLuck" and invoked function with this argument, (5) means that googLuck = 5;
